
# Kashish Pizza assignment 1 Mobile applications development
Sourced from
## <a href="https://github.com/rhildred/ES6OrderBot" target="_blank">ES6 Order Bot</a>

To run:

1. The first time run `npm install`
2. Press ctrl-f5 while your focus is in one of the files that starts with a number and is all lower case.
3. Open the page localhost:xxxx (port displayed in your console. Set to 3002)

## Kashish Pizza chat bot
Order bot for a pizza store. 
The menu has 3 items (Pizza, Burger, Sub).
If you choose Pizza, you'll have 3 options for size.
You can choose your toppings, your choice of dips and a drink.
Upon completion, you'll get an approximate total and a time to pick-up the order.

## Why MIT?
The MIT license is a great choice because it allows you to share your code under a copyleft license without forcing others to expose their proprietary code, it’s business friendly and open source friendly while still allowing for monetization.

The MIT license is one of the most permissive software licenses out there. It’s not only friendly to open source developers but also to businesses. Unlike other licenses like the GPL that require you to make a copy of the source code and all dependencies to any user of the software, the MIT allows you to keep your source code private so long as you give credit in the license file along with a copyright notice.